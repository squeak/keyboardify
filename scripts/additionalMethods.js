var _ = require("underscore");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  IS FOCUSED ELEMENT AN INPUT ?
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/*
  DESCRIPTION: check if the currently focused dom element is an input, textarea, or is editable
  ARGUMENTS: ( ø )
  RETURN: <boolean>
*/
function isFocusInput () {
  return !(
    (document.activeElement.tagName != "INPUT" && document.activeElement.tagName != "TEXTAREA")
    && document.activeElement.isContentEditable != true
  );
};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (keyboardify, keyboardjs) {
  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  VARIABLES
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: list of previously used contexts names, in order of most recent usage
      TYPE: <string[]>
    */
    contexts: ["global"],

    /**
      DESCRIPTION: lists of saved shortcuts and their descriptions
      TYPE: <{
        !keycomboId: <string> « an id to allow to tag identical keycombos in different contexts, so that if one is modified all others with same id will as well be »,
        !keycombo <string|<string>[]> « see keyboardjs docs for detailed documentation on the available key combo syntaxes »,
        !context: <string> « shortcut context »,
        ?pressHandler <function(e)> « callback method to run when key is pressed »,
        ?releaseHandler <function(e)> « callback method to run when key is released »,,
        ?preventRepeatByDefault <boolean> « if true, won't repeat the press handler if key stays pressed »,
        ?description: <string> « a description of the effect of the keycombo, as it should be displayed to the user »,
        ... any value you chose to store in saveToList
      }>
    */
    list: [],

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  CANCEL CONTEXT
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: remove a context from contexts history, if it's currently active, activate last active one instead
      ARGUMENTS: ( !contextToCancel <string>, )
      RETURN: <void>
    */
    cancelContext: function (contextToCancel) {
      keyboardify.contexts = _.reject(keyboardify.contexts, function (x) { return x == contextToCancel; });
      if (keyboardify.getContext() == contextToCancel) keyboardify.setContext(keyboardify.contexts[0]);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  GET DESCRIPTION TEXTS
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: get description texts for a button
      ARGUMENTS: (
        <string|{ keycombo: <string>, description: <string>, }>
      )
      RETURN: <{
        inlineTitle: <string> « description text if displayed inline »,
        bubbleShortcut: <string> « the bubble description of the shortcut for an inline title »,
        bubbleTitleAndShortcut: <string> « all in the bubble: title description and shortcut »,
      }>
    */
    getDescriptionTexts: function (keycomboId_or_keycomboAndDescription) {

      // GET KEYCOMBO AND DESCRIPTION VALUES EITHER FROM KEYBOARDIFY.LIST OR FROM PASSED ARGUMENT
      if (_.isString(keycomboId_or_keycomboAndDescription)) {

        // get combo object from list
        var keycomboObject = _.findWhere(keyboardify.list, {
          keycomboId: keycomboId_or_keycomboAndDescription,
          context: keyboardify.getContext(),
        });
        if (!keycomboObject) keycomboObject = _.findWhere(keyboardify.list, { keycomboId: keycomboId_or_keycomboAndDescription, });
        if (!keycomboObject) return console.error("[keyboardify] Could not find keycombo object in keyboardify.list, with id: "+ keycomboId_or_keycomboAndDescription);

        var keycomboAndDescription = _.clone(keycomboObject);

      }
      else var keycomboAndDescription = _.clone(keycomboId_or_keycomboAndDescription);

      // IF KEYCOMBO IS A SINGLE LETTER, UNDERLINE IT IF INLINE TITLE
      if (
        _.isString(keycomboAndDescription.keycombo)
        && keycomboAndDescription.keycombo.length === 1
      ) var inlineTitle = keycomboAndDescription.description.replace(
        new RegExp (keycomboAndDescription.keycombo, "g"),
        // for g, j, p, y and q, make sure underline position is lower, otherwise it's not visible
        "<u"+ ("gjpyq".match(keycomboAndDescription.keycombo) ? ' style="text-underline-position: under;"' : '') +">"+ keycomboAndDescription.keycombo +"</u>"
      )
      else var inlineTitle = keycomboAndDescription.description;

      // RETURN AN OBJECT CONTAINING VARIOUS POSSIBLE STYLES OF TITLES
      return {
        inlineTitle: inlineTitle,
        bubbleShortcut: keycomboAndDescription.keycombo,
        bubbleTitleAndShortcut: keycomboAndDescription.description +"\n("+ keycomboAndDescription.keycombo +")",
      };
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  MODIFIED BINDINGS AND LOCAL STORAGE
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: retrieve modified config for a keycombo from localStorage
      ARGUMENTS: (
        keycomboId <string> « id of the keycombo for which to retrieve modified config »
      )
      RETURN: keycomboObject <{
        description: <string>,
        keycombo: <string|string[]>,
      }>
    */
    getSavedModifiedBinding: function (keycomboId) {
      if (!keycomboId) return;
      var modifiedShortcutsList = JSON.parse(localStorage.getItem("keyboardify_modified_shortcuts")) || {};
      return modifiedShortcutsList[keycomboId];
    },

    /**
      DESCRIPTION: save a modified binding into localStorage
      ARGUMENTS: (
        keycomboId <string> « id of the keycombo for which to add modified config »
        keycomboObject <{
          description: <string>,
          keycombo: <string|string[]>,
          ...
        }>
      )
      RETURN: <void>
    */
    addModifiedBindingToLocalStorage: function (keycomboId, keycomboObject) {
      var modifiedShortcutsList = JSON.parse(localStorage.getItem("keyboardify_modified_shortcuts")) || {};
      modifiedShortcutsList[keycomboId] = keycomboObject; // functions and undefined values will be removed by JSON.stringify, so this will be cleaned out from useless parts
      localStorage.setItem("keyboardify_modified_shortcuts", JSON.stringify(modifiedShortcutsList));
    },

    /**
      DESCRIPTION: clear one or all modified bindings from localStorage
      ARGUMENTS: (
        ?keycomboId <string> «
          if defined, will remove the specified combo id from localStorage
          if not defined, will remove all customized shortcuts from localStorage
        »,
      )
      RETURN: <void>
    */
    clearSavedModifiedBinding: function (keycomboId) {
      var modifiedShortcutsList = JSON.parse(localStorage.getItem("keyboardify_modified_shortcuts")) || {};
      if (keycomboId) {
        if (modifiedShortcutsList[keycomboId]) {
          keyboardify.modifyBind(keycomboId);
          delete modifiedShortcutsList[keycomboId];
          localStorage.setItem("keyboardify_modified_shortcuts", JSON.stringify(modifiedShortcutsList));
        };
      }
      else {
        _.each(modifiedShortcutsList, function (shortcutKeycomboObject, keycomboId) {
          keyboardify.modifyBind(keycomboId);
        });
        localStorage.removeItem("keyboardify_modified_shortcuts");
      };
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  CUSTOMIZE HANDLERS
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: customize handlers so that they get triggered only if focused element is not and input, textarea or editable
      ARGUMENTS: (
        ?pressHandler <function(e)> « callback method to run when key is pressed »,
        ?releaseHandler <function(e)> « callback method to run when key is released »,,
      )
      RETURN: <{
        ?newPressHandler <function(e)> « pressHandler modified so that it acts only if focused dom element is not an input, textarea or editable »,
        ?newReleaseHandler <function(e)> « releaseHandler modified so that it acts only if focused dom element is not an input, textarea or editable »,
      }>
    */
    customizeHandlers: function (pressHandler, releaseHandler) {
      return {
        press: pressHandler ? function () {
          if (!isFocusInput()) return pressHandler.apply(this, arguments);
        } : undefined,
        release: releaseHandler ? function () {
          if (!isFocusInput()) return releaseHandler.apply(this, arguments);
        } : undefined,
      };
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  MODIFY BIND
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: modify a binding saved in list
      ARGUMENTS: (
        !keycomboId <string> « id of the keycombo to modify »,
        !newKeyComboOrOptions <string|object> «
          if a string, will simply modify the key strokes necessary to run the handlers without modifying any other options
          if an object, all object properties will replace the ones of the keycombo object saved (this way you can modify as well handlers, description...)
        »,
        ?saveToLocalStorage <boolean> « if this is set to true, will store that modification to localStorage, so that on next visits to this page, when the default shortcut is requested, keyboardify will automatically use the customized one »,
      )
      RETURN: newKeycomboObject <object>
    */
    modifyBind: function (keycomboId, newKeyComboOrOptions, saveToLocalStorage) {

      // make sure newKeyComboOrOptions is an object
      if (_.isString(newKeyComboOrOptions)) newKeyComboOrOptions = { keycombo: newKeyComboOrOptions, };

      // get all binds with same id to modify them

      _.chain(keyboardify.list)
        .where({ keycomboId: keycomboId, })
        .each(function (keycomboObject) {

          // switch to context of the combo
          if (keycomboObject.context) {
            var previousContext = keyboardjs.getContext();
            keyboardjs.setContext(keycomboObject.context);
          };

          // unbind keyboardjs combo
          keyboardjs.unbind(keycomboObject.keycombo, keycomboObject.modifiedPressHandler, keycomboObject.modifiedReleaseHandler);

          // reverse modification of keycomboObject
          if (!newKeyComboOrOptions) {
            var keycomboOptionsToSetup = keycomboObject.defaults;
            delete keycomboObject.bindingManuallyModified;
            // empty keycomboObject before refilling it afterwards
            _.each(keycomboObject, function (value, key) { delete keycomboObject[key]; });
          }
          // save defaults
          else {
            if (!keycomboObject.bindingManuallyModified) keycomboObject.defaults = _.clone(keycomboObject);
            keycomboObject.bindingManuallyModified = true;
            var keycomboOptionsToSetup = newKeyComboOrOptions;
          };

          // modify keycomboObject
          _.each(keycomboOptionsToSetup, function (newKeycomboObjectValue, newKeycomboObjectKey) {
            keycomboObject[newKeycomboObjectKey] = newKeycomboObjectValue;
          });

          // in case handlers have been modified, recustomize them
          var newHandlers = keyboardify.customizeHandlers(keycomboObject.pressHandler, keycomboObject.releaseHandler);
          keycomboObject.modifiedPressHandler = newHandlers.press;
          keycomboObject.modifiedReleaseHandler = newHandlers.release;

          // rebind keyboardjs keycombo
          keyboardjs.bind(keycomboObject.keycombo, keycomboObject.modifiedPressHandler, keycomboObject.modifiedReleaseHandler, keycomboObject.preventRepeatByDefault);

          // switch back to previous context
          if (previousContext) keyboardjs.setContext(previousContext);

        })
      ;

      // if modification should be saved, add it to localStorage (on top of existing one, so that if modified multiple times in a row, localStorage has the right list of modifications)
      if (saveToLocalStorage) keyboardify.addModifiedBindingToLocalStorage(
        keycomboId,
        Object.assign({ bindingManuallyModified: true, }, keyboardify.getSavedModifiedBinding(keycomboId), newKeyComboOrOptions)
      );

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };
};
