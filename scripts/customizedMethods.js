var _ = require("underscore");

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

module.exports = function (keyboardify, keyboardjs) {
  return {
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  BIND
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: bind a keycombo to some handlers, but make it only work if focused element is not and input, textarea or editable
      ARGUMENTS: (
        !keycombo <string|<string>[]> « see keyboardjs docs for detailed documentation on the available key combo syntaxes »,
        ?pressHandler <function(e)> « callback method to run when key is pressed »,
        ?releaseHandler <function(e)> « callback method to run when key is released »,,
        ?preventRepeatByDefault <boolean> « if true, won't repeat the press handler if key stays pressed »,
        ?saveToList <{
          !keycomboId: <string> « an id to allow to tag identical keycombos in different contexts, so that if one is modified all others with same id will as well be »,
          ?description: <string> « a description of the effect of the keycombo, as it should be displayed to the user »,
          ... any other info you want to store in the keycombo object stored in keyboardify.list
        }> « if this is defined, the keycombo, handlers and all info in this object will be stored in keyboardify.list, so they can easily be inventoried and modified »,
        ?multibind <boolean> « if true, will not unbind key combo from previous handlers before binding it to the newly passed handlers »
      )
      RETURN: <keyboardjs.bind·return>
    */
    bind: function (keyComboStr, pressHandler, releaseHandler, preventRepeatByDefault, saveToList, multibind) {

      // modify pressHandler so it's executed only if focused dom element is not editable
      var newHandlers = keyboardify.customizeHandlers(pressHandler, releaseHandler);

      // retrieve customized keycomboObject from localStorage, if it has been modified
      if (saveToList && saveToList.keycomboId) var keycomboObjectSavedInLocalStorage = keyboardify.getSavedModifiedBinding(saveToList.keycomboId);

      // create final keycomboObject
      var finalKeycomboObject = Object.assign(
        {
          keycombo: keyComboStr,
          preventRepeatByDefault: preventRepeatByDefault,
        },
        saveToList,
        keycomboObjectSavedInLocalStorage,
        {
          pressHandler: pressHandler,
          releaseHandler: releaseHandler,
          modifiedPressHandler: newHandlers.press,
          modifiedReleaseHandler: newHandlers.release,
          context: keyboardify.getContext(),
        }
      );

      // make sure to unbind key combo before binding it again
      if (!multibind) keyboardify.unbind(finalKeycomboObject.keycombo, undefined, undefined, saveToList ? saveToList.keycomboId : undefined);

      // if requested save combo to keyboardify.list
      if (saveToList) keyboardify.list.push(finalKeycomboObject);

      // run normal keyboardjs method
      return keyboardjs.bind.call(
        keyboardjs,
        finalKeycomboObject.keycombo,
        finalKeycomboObject.modifiedPressHandler,
        finalKeycomboObject.modifiedReleaseHandler,
        finalKeycomboObject.preventRepeatByDefault
      );

    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  UNBIND
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: unbind a keycombo, making sure that if it is escape, the default escape binding won't be lost
      ARGUMENTS: (
        !keycombo <string|<string>[]> « see keyboardjs docs for detailed documentation on the available key combo syntaxes »,
        ?pressHandler <function(e)> « callback method for which to cancel press binding »,
        ?releaseHandler <function(e)> « callback method for which to cancel release binding »,
        ?keycomboId <string> «
          if the combo was saved in shortcuts list, pass here it's id to remove it from the list
          will remove only combos matching this id in the current context
        »,
      )
      RETURN: <keyboardjs.unbind·return>
    */
    unbind: function (keyComboStr, pressHandler, releaseHandler, keycomboId) {

      // run normal unbind function
      var unbindReturn = keyboardjs.unbind.call(keyboardjs, keyComboStr, pressHandler, releaseHandler);

      // always rebind esc to unfocusing from current dom element
      if (keyComboStr == "esc" || keyComboStr == "escape") {
        keyboardjs.unbind("esc");
        keyboardjs.bind("esc", function () { document.activeElement.blur() });
      }

      // remove from list if asked
      if (keycomboId) keyboardify.list = _.reject(keyboardify.list, function (keycomboObject) {
        return keycomboObject.keycomboId == keycomboId && keycomboObject.context == keyboardify.getContext();
      });

      // return keyboardjs unbind return
      return unbindReturn;
      
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //                                                  SET CONTEXT
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

    /**
      DESCRIPTION: very similar to keyboardjs.setContext, except that it saves contexts switching history in keyboardify.contexts
      ARGUMENTS: ( contextName <string> )
      RETURN: <keybaordjs.setcontext·return>
    */
    setContext: function (contextName) {
      keyboardify.contexts.unshift(contextName);
      keyboardjs.setContext.apply(keyboardjs, arguments);
    },

    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
    //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  };
};
