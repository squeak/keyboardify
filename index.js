var _ = require("underscore");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INITIALIZE KEYBOARDJS AND KEYBOARDIFY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// create a dummy window object in case it doesn't exist, to support being imported server side, to avoid errors
if (typeof window == "undefined") window = {};

// use existing keyboardjs instance, or create it if not done already (so that if keyboardjs has already been imported, will use the global one)
window.keyboardjs = window.keyboardjs || require("keyboardjs");

// ⚠⚠⚠                                                                                                                     ⚠⚠⚠
// ⚠⚠⚠ IF YOU HAVE TO DEBUG SHIT IN KEYBOARDJS, BE AWARE THAT THE MODULE IS IMPORTING THE keyboardjs/dist/keyboard.js FILE ⚠⚠⚠
// ⚠⚠⚠   THIS IS SPECIFIED IN keyboardjs/package.json AND IS PROBABLY A WAY TO NOT HAVE ISSUES SUPPORTING EXPORT SYNTAX    ⚠⚠⚠
// ⚠⚠⚠                                                                                                                     ⚠⚠⚠

if (!window.keyboardify) {
  // o            >            +            #            °            ·            ^            :            |
  //                                           SETUP KEYBOARDIFY AS A COPY OF KEYBOARDJS, AND CUSTOMIZE IT'S LOCALE

  var keyboardjs = window.keyboardjs;
  keyboardjs.setLocale("xx", require("./scripts/customizedLocale"));
  window.keyboardify = Object.create(keyboardjs);
  var keyboardify = window.keyboardify;

  // o            >            +            #            °            ·            ^            :            |
  //                                           IMPROVE KEYBOARDIFY

  // customize some keyboardjs methods
  _.each(require("./scripts/customizedMethods")(keyboardify, keyboardjs), function (method, methodName) {
    keyboardify[methodName] = method;
  });

  // add some new methods
  _.each(require("./scripts/additionalMethods")(keyboardify, keyboardjs), function (method, methodName) {
    keyboardify[methodName] = method;
  });

  // bound "esc" key to unfocusing from current dom element
  keyboardjs.bind("esc", function () {
    document.activeElement.blur()
  });

  // o            >            +            #            °            ·            ^            :            |
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = window.keyboardify;
