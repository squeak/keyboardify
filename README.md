# keyboardify

Easily add shortcuts to your application (a simple wrapper to keyboardjs).

For the full documentation, installation instructions... check [keyboardify documentation page](https://squeak.eauchat.org/libs/keyboardify/).
