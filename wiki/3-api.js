const _ = require("underscore");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// get lib code to pass to api maker (this is useful for automatic listing of methods)
const keyboardify = require("../index");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {

  title: "API",
  type: "api",

  moduleName: "keyboardify",

  documentation: {
    methods: [
      //
      //                              CUSTOMIZED METHODS

      {
        display: {
          title: "CUSTOMIZED METHODS",
          comment: "Customized methods (present in keyboardjs but their behaviour has been extended).",
          borderColor: "primary",
        },
        path: "scripts/customizedMethods",
        methodsAutofill: {
          type: "all",
          prefix: "keyboardify",
        },
      },

      //
      //                              ADDITIONAL METHODS

      {
        display: {
          title: "ADDITIONAL METHODS",
          comment: "Additional methods (not present in keyboardjs).",
          borderColor: "secondary",
        },
        path: "scripts/additionalMethods",
        methodsAutofill: {
          type: "all",
          prefix: "keyboardify",
        },
      },

      //                              ¬
      //

    ],

  },

};
