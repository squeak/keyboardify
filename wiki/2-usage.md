# Usage

To use the library, import it in your scripts with:

```javascript
const keyboardify = require("keyboardify");
```

You can now use it in your page like this:

```javascript
keyboardify.bind("a", function (e) { console.log('"a" has been pressed'); })
```
