
Keyboardify is a wrapper of [keyboardjs](https://www.npmjs.com/package/keyboardjs).
It's goal is to unify the use of one keyboardjs instance when multiple libraries need it in a single app.

Keyboardify has some peculiarities compared to keyboardjs:
  - shortcuts won't be triggered if you're in an input, textarea or editable element (so that you can use shift as mod key)
  - whatever you do, if you're in an input, textarea..., esc will always primarily let you "unfocus" that input (to make it easy to go to shortcuts mode if you're in an input)

Keyboardify also adds a few new functionalities and methods to make it easier to switch contexts, make list(s) of all shortcuts used, make it easy to modify some shortcuts on the go (making easy to have shortcuts editable by the user).
